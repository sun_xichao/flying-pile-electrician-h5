// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Bus from 'vue-bus'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import fastClick from 'fastclick'
import VueScroller from 'vue-scroller'
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'

import './assets/css/iconfont/iconfont.css'
import {VueJsonp} from 'vue-jsonp'
import Vant from 'vant'
import 'vant/lib/index.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 全局引入bridge
import Bridge from './assets/bridge/bridge'

Vue.prototype.$ajax = axios
Vue.config.productionTip = false
Vue.use(VueScroller)
Vue.use(Bus)
Vue.use(Mint)
Vue.use(Vant)
Vue.use(ElementUI)
Vue.use(VueJsonp)
fastClick.attach(document.body)
Vue.prototype.$bridge = Bridge

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
