import Vue from 'vue'
import Router from 'vue-router'
import home from '@/pages/home/home'
import addLog from '@/pages/home/addLog'
import toll from '@/pages/toll/toll'
import tollDetail from '@/pages/toll/tollDetail'
import task from '@/pages/task/task'
import taskDetail from '@/pages/task/taskDetail'
import app from '@/pages/application/app'
import college from '@/pages/application/college'
import collegeList from '@/pages/application/collegeList'
import videoPlay from '@/pages/application/videoPlay'
import paperList from '@/pages/application/paperList'
import paperRecordList from '@/pages/application/paperRecordList'
import paper from '@/pages/application/paper'
import paperDetail from '@/pages/application/paperDetail'
import paperRecordDetail from '@/pages/application/paperRecordDetail'
import my from '@/pages/my/my'
import account from '@/pages/my/account/account'
import billList from '@/pages/my/account/billList'
import point from '@/pages/my/point/point'
import pointList from '@/pages/my/point/pointList'
import card from '@/pages/my/account/card'
import cardView from '@/pages/my/account/cardView'
import myInfo from '@/pages/my/myInfo'
import aboutUs from '@/pages/my/components/aboutUs'
import back from '@/pages/components/back'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/addLog',
      name: 'addLog',
      component: addLog
    },
    {
      path: '/toll',
      name: 'toll',
      component: toll
    },
    {
      path: '/tollDetail',
      name: 'tollDetail',
      component: tollDetail
    },
    {
      path: '/task',
      name: 'task',
      component: task
    },
    {
      path: '/taskDetail',
      name: 'taskDetail',
      component: taskDetail
    },
    {
      path: '/application',
      name: 'app',
      component: app
    },
    {
      path: '/college',
      name: 'college',
      component: college
    },
    {
      path: '/videoPlay',
      name: 'videoPlay',
      component: videoPlay
    },
    {
      path: '/collegeList',
      name: 'collegeList',
      component: collegeList
    },
    {
      path: '/paperList',
      name: 'paperList',
      component: paperList
    },
    {
      path: '/paperRecordList',
      name: 'paperRecordList',
      component: paperRecordList
    },
    {
      path: '/paper',
      name: 'paper',
      component: paper
    },
    {
      path: '/paperDetail',
      name: 'paperDetail',
      component: paperDetail
    },
    {
      path: '/paperRecordDetail',
      name: 'paperRecordDetail',
      component: paperRecordDetail
    },
    {
      path: '/my',
      name: 'my',
      component: my
    },
    {
      path: '/account',
      name: 'account',
      component: account
    },
    {
      path: '/billList',
      name: 'billList',
      component: billList
    },
    {
      path: '/point',
      name: 'point',
      component: point
    },
    {
      path: '/pointList',
      name: 'pointList',
      component: pointList
    },
    {
      path: '/card',
      name: 'card',
      component: card
    },
    {
      path: '/cardView',
      name: 'cardView',
      component: cardView
    },
    {
      path: '/myInfo',
      name: 'myInfo',
      component: myInfo
    },
    {
      path: '/aboutUs',
      name: 'aboutUs',
      component: aboutUs
    },
    {
      path: '/back',
      name: 'back',
      component: back
    }
  ]
})
