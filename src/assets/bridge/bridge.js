function setupWebViewJavascriptBridge (callback) {
  if (window.WebViewJavascriptBridge) {
    return callback(window.WebViewJavascriptBridge)
  }
  if (window.WVJBCallbacks) {
    return window.WVJBCallbacks.push(callback)
  }
  window.WVJBCallbacks = [callback]
  let WVJBIframe = document.createElement('iframe')
  WVJBIframe.style.display = 'none'
  WVJBIframe.src = 'https://__bridge_loaded__'
  document.documentElement.appendChild(WVJBIframe)
  setTimeout(() => {
    document.documentElement.removeChild(WVJBIframe)
  }, 0)
}
function setupWKWebViewJavascriptBridge (callback) {
  console.log('callback 是', callback)
  if (window.WKWebViewJavascriptBridge) { return callback() }
  if (window.WKWVJBCallbacks) { return window.WKWVJBCallbacks.push(callback) }
  window.WKWVJBCallbacks = [callback]
  window.webkit.messageHandlers.iOS_Native_InjectJavascript.postMessage(null)
}
export default {
  callhandlers (name, data, callback) {
    setupWKWebViewJavascriptBridge(function (bridge) {
      bridge.callhandler(name, data, callback)
    })
  },
  registerhandlers (name, callback) {
    setupWKWebViewJavascriptBridge(function (bridge) {
      bridge.registerhandler(name, function (data, responseCallback) {
        callback(data, responseCallback)
      })
    })
  },
  callhandler (name, data, callback) {
    setupWebViewJavascriptBridge(function (bridge) {
      bridge.callHandler(name, data, callback)
    })
  },
  registerhandler (name, callback) {
    setupWebViewJavascriptBridge(function (bridge) {
      bridge.registerHandler(name, function (data, responseCallback) {
        callback(data, responseCallback)
      })
    })
  }
}
