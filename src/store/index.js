import vue from 'vue'
import Vuex from 'Vuex'

vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: 'rlbz/m4C4AoXfpz63x6ZzA8fHnBGT8A/vxv4Ed/8cm8=',
    ids: '0000', //
    distance: '0',
    loading: false
  },
  // store 同步应用
  mutations: {
    set_token (state, params) {
      state.token = params
    },
    updateIds (state, msg, msg2) {
      state.ids = msg
      state.distance = msg2
    },
    showLoading (state) {
      state.Loading = true
    },
    hideLoading (state) {
      state.Loading = false
    }
  },
  // store 异步应用
  actions: {
  }
})
