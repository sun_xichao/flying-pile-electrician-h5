# 飞桩电工-H5

#### Description
公司电工端小程序，通过平台派单，电工端接单，在电工端进行相应工地流程处理，平台实时管控电工端施工质量，具有对账单功能，小程序端实时信息提醒，通过梳理业务了流程，使用vue框架，进行h5端输出。具体使用技术包含store、scss、vue、axios、vue-router、webpack、vue-loader、Vant

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
